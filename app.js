const fs = require('fs');
const os = require('os');
const yargs = require('yargs');
const _ = require('lodash')

const notes = require('./notes')

var numbers = [2,3,1,5,4,2,2,6,7,8,5,4,3,2,1,6,8]
// console.log(_.uniq(numbers))
// console.log(process.argv,yargs.argv)
// var user = os.userInfo();
// fs.appendFileSync('greetings.txt', `Hello ${user.username} age ${notes.age}!`);
// console.log(notes.addNumbers(10,5))
var titleOptions = {
    describe: 'Title of note',
    demand: true,
    alias: 't'
}
var descOptions = {
    describe: 'Body of note',
    demand: true,
    alias: 'b'
}
var argv = yargs
    .command('add','Add a new note',{
        title: titleOptions,
        body:descOptions,
    })
    .command('read','Read selected notes',{
        title: titleOptions,
    })
    .command('remove','Remove a note',{
        title: titleOptions,
    })
    .command('list','List all the notes available',{})
    .help()
    .argv

    console.log(titleOptions,descOptions)
switch (process.argv[2]) {
    case 'add':
        notes.addNote(argv.title,argv.body)
        break;
    case 'list':
        notes.getAll()
        break;
    case 'remove':
        notes.removeNote(argv.title)
        break;
    case 'read':
        notes.getNote(argv.title)
        break;

    default:
        console.log(
            `The command '${process.argv[2]}' is not recognized, check if the spelling is correct. The available commands are \n`+
            '\tadd -t<note-title> -d<note-description> - Used to add a new note\n'+
            '\tread <note-title> - Used to read a note that matches the provided title\n'+
            '\tremove <note-title> - Used to delete a note that matches the provided title\n'+
            '\tlist - Used to list all the notes stored\n'
        );
        break;
}