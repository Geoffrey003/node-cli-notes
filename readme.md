# Notes CLI
A simple node app to be run on the commmand line interface.

## Description
This app allows you to create, delete and view your notes. It provides validation mechanisms that prevents a user from making mistakes, it also provides help functionality.

### Creating a note
Creates a notes and saves it for you. A note has a title and a body. You can provide `--title` or `-t` commands for your title and `--body` or `-b` for the body.

```bash
    node app add -t "hello world" -b "Welcome to your first note"
```

### Listing notes
The list command take no arguments except the `--help` command
``` bash
    node app list
```
The results will be
```text
    Your Notes
    ---------------
            Title: hello world,
            Body: Welcome to your first note
```

### Removing notes
Removing a note is easy, all you provide is a title and all notes that match that title will be removed
```bash 
    node app remove -t "hello" 
```

### Reading notes
You can filter  out listed notes by using the read command.

```bash
    node app read -t "hello"
```

The results fetched will be
```text
Results
--------------
        Title: hello world,
        Body: Welcome to your first note

        Title: hello world 2,
        Body: Welcome to your first note 2
```

Try it out quite cool!