// module.exports.age = 25 
// module.exports.addNote = () => {
//     return 'New Note.'
// }

// module.exports.addNumbers = (a,b) => {
//     return a+b
// }
const fs = require('fs')
const _ = require('lodash')
var notes = []

//Write the notes to the file
function writeNotes(notes) {
    fs.writeFileSync('notes.json',JSON.stringify(notes))
}

//Read all the notes and if file does not exists create it
try{
    notes = JSON.parse(fs.readFileSync('notes.json'))
}catch(ex){
    writeNotes(notes)
}

var addNote = (title,body) => {
    var note = {title,body}
    notes.push(note)
    writeNotes(notes)
    console.log(`Added Note \n---------------\nTitle: ${title}\nBody: ${body}`)
}
var getAll = () => {
    console.log("Your Notes \n---------------")
    notes.forEach(note => {
        console.log(`\tTitle: ${note.title},\n\tBody: ${note.body}\n`)
    });
}
var getNote = (title) => {
    let found = false
    console.log("Results \n--------------")
    notes.forEach(note => {
        if(note.title.includes(title)){
            found = true
            console.log(`\tTitle: ${note.title},\n\tBody: ${note.body}\n`)
        }
    });

    //if no results were found
    if(!found){
        console.log("There were no notes that match the title provided, try to remove some words from the title.\n")
    }
}

var removeNote = (title) => {
    var len = notes.length
   _.remove(notes,(note) => {
       return note.title.includes(title)
   })

   if(len == notes.length){
       console.log("There were no notes that match the title provided, try to remove some words from the title.\n")
   }else{
       writeNotes(notes)
       console.log(`Successfully removed ${len - notes.length} item(s)!\n`)
   }
}

module.exports = {
    addNote,
    getAll,
    getNote,
    removeNote
}